﻿using System;
using System.Collections.Generic;

namespace Sdm.Leader.Data.Models;

public partial class User
{
    public long Id { get; set; }

    public int LevelId { get; set; }

    public UserRole? Role { get; set; }

    public string Name { get; set; } = null!;

    public virtual UserLevel Level { get; set; } = null!;

    public virtual ICollection<NotificationRecipient> NotificationRecipients { get; set; } = new List<NotificationRecipient>();

    public virtual ICollection<UserNotification> UserNotifications { get; set; } = new List<UserNotification>();
}
