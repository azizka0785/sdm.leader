﻿using System;
using System.Collections.Generic;

namespace Sdm.Leader.Data.Models;

public partial class NotificationRecipient
{
    public long Id { get; set; }

    public UserRole? UserRole { get; set; }

    public int? UserLevelId { get; set; }

    public long? UserId { get; set; }

    public virtual User? User { get; set; }

    public virtual UserLevel? UserLevel { get; set; }
}
