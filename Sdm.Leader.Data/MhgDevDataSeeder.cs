﻿using Microsoft.EntityFrameworkCore;
using Sdm.Leader.Data.Models;

namespace Sdm.Leader.Data
{
    public partial class MhgDevContext
    {
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserLevel>().HasData
            (
                new UserLevel 
                { 
                    Id = 1,
                    Description = "Full access"
                },
                new UserLevel
                {
                    Id = 2,
                    Description = "Only read notifications"
                }
            );

            modelBuilder.Entity<User>().HasData
            (
                new User
                {
                    Id = 1,
                    LevelId = 1,
                    Role = UserRole.Administrator,
                    Name = "admin"
                },
                new User
                {
                    Id = 2,
                    LevelId = 2,
                    Role = UserRole.Marketer,
                    Name = "market"
                }
            );

            modelBuilder.Entity<NotificationRecipient>().HasData
            (
                new NotificationRecipient 
                { 
                    Id = 1,
                    UserRole = UserRole.Sales,
                    UserLevelId = 2,
                    UserId = 1
                }
            );
        }
    }
}
