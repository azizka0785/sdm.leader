﻿namespace Sdm.Leader.Data.Models
{
    public enum UserRole
    {
        Administrator,
        Distributor,
        Marketer,
        Sales
    }
}
