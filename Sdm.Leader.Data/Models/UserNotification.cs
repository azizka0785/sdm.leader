﻿using System;
using System.Collections.Generic;

namespace Sdm.Leader.Data.Models;

public partial class UserNotification
{
    public long Id { get; set; }

    public long UserId { get; set; }

    public long NotificationId { get; set; }

    public virtual Notification Notification { get; set; } = null!;

    public virtual User User { get; set; } = null!;
}
