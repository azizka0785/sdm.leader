﻿namespace Sdm.Leader.Service.DTO
{
    public class NotificationDTO
    {
        public long Id { get; set; }

        public string? Data { get; set; }
    }
}
