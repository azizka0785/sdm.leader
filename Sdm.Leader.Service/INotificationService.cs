﻿using Sdm.Leader.Service.DTO;

namespace Sdm.Leader.Service
{
    public interface INotificationService
    {
        Task<NotificationDTO?> GetAsync(long id, CancellationToken cancellationToken);

        void Create(NotificationDTO notification, CancellationToken cancellationToken);
    }
}
