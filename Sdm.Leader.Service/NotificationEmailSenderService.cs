﻿using Microsoft.Extensions.Logging;
using Sdm.Leader.Data.Models;

namespace Sdm.Leader.Service
{
    public sealed class NotificationEmailSenderService : INotificationSenderService
    {
        private readonly ILogger<NotificationEmailSenderService>? _logger;

        public NotificationEmailSenderService(ILogger<NotificationEmailSenderService>? logger)
        {
            _logger = logger;
        }

        public Task SendAsync(User user, Notification notification, CancellationToken cancellationToken)
        {
            _logger?.LogInformation("Notification sended, user={UserName}, notification={NotificationData}", user.Name, notification.Data);
        
            return Task.CompletedTask;
        }
    }
}
