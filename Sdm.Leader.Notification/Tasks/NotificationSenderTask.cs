﻿using Sdm.Leader.Service;

namespace Sdm.Leader.Notification.Tasks
{
    public sealed class NotificationSenderTask : IHostedService
    {
        private readonly INotificationCheckerService _notificationCheckerService;
        private readonly ILogger<NotificationSenderTask>? _logger;        

        public NotificationSenderTask(INotificationCheckerService notificationCheckerService, ILogger<NotificationSenderTask>? logger)
        {            
            _notificationCheckerService = notificationCheckerService;
            _logger = logger;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _notificationCheckerService.Check(cancellationToken);

            _logger?.LogInformation("NotificationSenderTask started");

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _notificationCheckerService.Stop(cancellationToken);

            _logger?.LogInformation("NotificationSenderTask stoped");

            return Task.CompletedTask;
        }      
    }
}
