﻿using System;
using System.Collections.Generic;

namespace Sdm.Leader.Data.Models;

public partial class Notification
{
    public long Id { get; set; }

    public string? Data { get; set; }

    public virtual ICollection<UserNotification> UserNotifications { get; set; } = new List<UserNotification>();
}
