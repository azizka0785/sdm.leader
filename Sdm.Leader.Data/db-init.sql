CREATE TYPE public."user_role" AS ENUM (
	'administrator',
	'distributor',
	'marketer',
	'sales');


CREATE TABLE public.notifications (
	id bigserial NOT NULL,
	"data" text NULL,
	CONSTRAINT notifications_pkey PRIMARY KEY (id)
);


CREATE TABLE public.user_levels (
	id int4 NOT NULL,
	description text NULL,
	CONSTRAINT user_levels_pkey PRIMARY KEY (id)
);


CREATE TABLE public."user" (
	id bigserial NOT NULL,
	level_id int4 NOT NULL,
	"role" public."user_role" NULL,
	"name" varchar(50) NOT NULL,
	CONSTRAINT user_pkey PRIMARY KEY (id)
);

ALTER TABLE public."user" ADD CONSTRAINT user_fk FOREIGN KEY (level_id) REFERENCES public.user_levels(id);


CREATE TABLE public.notification_recipients (
	id bigserial NOT NULL,
	"user_role" public."user_role" NULL,
	user_level_id int4 NULL,
	user_id int8 NULL,
	CONSTRAINT notification_recipients_pkey PRIMARY KEY (id)
);

ALTER TABLE public.notification_recipients ADD CONSTRAINT notification_recipients_user_fk FOREIGN KEY (user_id) REFERENCES public."user"(id);
ALTER TABLE public.notification_recipients ADD CONSTRAINT notification_recipients_user_levels_fk FOREIGN KEY (user_level_id) REFERENCES public.user_levels(id);


CREATE TABLE public.user_notification (
	id bigserial NOT NULL,
	user_id int8 NOT NULL,
	notification_id int8 NOT NULL,
	CONSTRAINT user_notification_pkey PRIMARY KEY (id)
);

ALTER TABLE public.user_notification ADD CONSTRAINT user_notification_notifications_fk FOREIGN KEY (notification_id) REFERENCES public.notifications(id);
ALTER TABLE public.user_notification ADD CONSTRAINT user_notification_user_fk FOREIGN KEY (user_id) REFERENCES public."user"(id);


CREATE TABLE public.user_queue_notifications (
	user_id int8 NOT NULL,
	notification_id int8 NOT NULL,
	id bigserial NOT NULL,
	CONSTRAINT user_queue_notifications_pk PRIMARY KEY (id)
);

ALTER TABLE public.user_queue_notifications ADD CONSTRAINT user_queue_notifications_notifications_fk FOREIGN KEY (notification_id) REFERENCES public.notifications(id);
ALTER TABLE public.user_queue_notifications ADD CONSTRAINT user_queue_notifications_user_fk FOREIGN KEY (user_id) REFERENCES public."user"(id);


INSERT INTO public.user_levels (id,description) VALUES
	 (1,'Full access'),
	 (2,'Only read notifications');


INSERT INTO public."user" (level_id,"role","name") VALUES
	 (1,'administrator','admin'),
	 (2,'marketer','market');


INSERT INTO public.notification_recipients ("user_role",user_level_id,user_id) VALUES
	 ('sales',2,1);