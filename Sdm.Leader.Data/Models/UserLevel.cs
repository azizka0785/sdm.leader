﻿using System;
using System.Collections.Generic;

namespace Sdm.Leader.Data.Models;

public partial class UserLevel
{
    public int Id { get; set; }

    public string? Description { get; set; }

    public virtual ICollection<NotificationRecipient> NotificationRecipients { get; set; } = new List<NotificationRecipient>();

    public virtual ICollection<User> Users { get; set; } = new List<User>();
}
