﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Sdm.Leader.Data;
using Sdm.Leader.Data.Models;

namespace Sdm.Leader.Service
{
    public sealed class NotificationCheckerService : INotificationCheckerService
    {
        private readonly object _lock;

        private readonly IDbContextFactory<MhgDevContext> _contextFactory;
        private readonly INotificationSenderService _notificationSenderService;
        private readonly ILogger<NotificationCheckerService>? _logger;

        private Task? _task;
        private bool _stopRequested;

        public NotificationCheckerService(
            IDbContextFactory<MhgDevContext> contextFactory, 
            INotificationSenderService notificationSenderService, 
            ILogger<NotificationCheckerService>? logger)
        {
            _lock = new object();

            _contextFactory = contextFactory ?? throw new ArgumentNullException(nameof(contextFactory));
            _notificationSenderService = notificationSenderService ?? throw new ArgumentNullException(nameof(notificationSenderService));
            _logger = logger;

            _stopRequested = false;
        }

        public void Check(CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested || _stopRequested) return;

            lock (_lock)
            {
                _logger?.LogInformation("Wait previous task, time={Now}", DateTime.UtcNow);

                _task?.Wait(cancellationToken);

                _logger?.LogInformation("Creating new task");

                _task = CheckAsync(cancellationToken);

                _logger?.LogInformation("New task created");
            }            

        }

        public void Stop(CancellationToken cancellationToken)
        {
            lock (_lock)
            {
                _stopRequested = true;

                _logger?.LogInformation("Wait task to stop, time={Now}", DateTime.UtcNow);

                _task?.Wait(cancellationToken);

                _logger?.LogInformation("Stopped, time={Now}", DateTime.UtcNow);
            }
        }

        public async Task CheckAsync(CancellationToken cancellationToken)
        {
            using var context = _contextFactory.CreateDbContext();

            var queue = await context.UserQueueNotifications
                .Include(item => item.User)
                .Include(item => item.Notification)
                .ToListAsync(cancellationToken);

            foreach (var item in queue)
            {
                try
                {
                    context.UserQueueNotifications.Remove(item);

                    var userNotification = new UserNotification
                    {
                        NotificationId = item.NotificationId,
                        UserId = item.UserId
                    };

                    context.UserNotifications.Add(userNotification);

                    await _notificationSenderService.SendAsync(item.User, item.Notification, cancellationToken);

                    context.SaveChanges();
                }
                catch (Exception exception)
                {
                    _logger?.LogWarning
                    (
                        exception,
                        "Error on send message to userId={UserId}, notificationId={NotificationId}",
                        item.UserId,
                        item.NotificationId
                    );
                }
            }
        }
    }
}
