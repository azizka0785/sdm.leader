﻿using Sdm.Leader.Data.Models;

namespace Sdm.Leader.Service
{
    public interface INotificationSenderService
    {
        Task SendAsync(User user, Notification notification, CancellationToken cancellationToken);
    }
}
