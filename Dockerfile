FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["Sdm.Leader.Data/Sdm.Leader.Data.csproj", "Sdm.Leader.Data/Sdm.Leader.Data.csproj"]
COPY ["Sdm.Leader.Service/Sdm.Leader.Service.csproj", "Sdm.Leader.Service/Sdm.Leader.Service.csproj"]
COPY ["Sdm.Leader.Notification/Sdm.Leader.Notification.csproj", "Sdm.Leader.Notification/Sdm.Leader.Notification.csproj"]
RUN dotnet restore "Sdm.Leader.Notification/Sdm.Leader.Notification.csproj"
COPY . .
WORKDIR "/src/Sdm.Leader.Notification"
RUN dotnet build "Sdm.Leader.Notification.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Sdm.Leader.Notification.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Sdm.Leader.Notification.dll"]